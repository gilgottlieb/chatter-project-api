import {Container} from "inversify";
import {IUserProvider, UserProvider} from "./providers/UserProvider";
import {IMessagingProvider, MessagingProvider} from "./providers/MessagingProvider";


export const container = new Container();
container.bind<IUserProvider>("userProvider").to(UserProvider);
container.bind<IMessagingProvider>("messagingProvider").to(MessagingProvider);
