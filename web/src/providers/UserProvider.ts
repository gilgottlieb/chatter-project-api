import {injectable} from "inversify";

export interface IUserProvider {
    login(username: string): void;

    logout(): void;

    getUsername(): string | null;
}

@injectable()
export class UserProvider implements IUserProvider {

    public async login(username: string) {
        window.sessionStorage.setItem("username", username);
        window.dispatchEvent(new CustomEvent("on-registration"));
    }

    public async logout() {
        window.sessionStorage.removeItem("username");
        window.dispatchEvent(new CustomEvent("on-logout"));
    }

    public getUsername(): string | null {
        return window.sessionStorage.getItem("username");
    }
}
