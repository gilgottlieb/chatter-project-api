import MessageSet from "../components/chat/MessageSet";
import {injectable} from "inversify";

export interface IMessagingProvider {
    getMessages(): MessageSet[];
    addMessage(content: string, sender: string, isSentFromLocal: boolean): void;
}

@injectable()
export class MessagingProvider implements IMessagingProvider{
    private readonly messageSets: MessageSet[];

    constructor() {
        this.messageSets = [];
    }

    public getMessages(): MessageSet[] {
        return this.messageSets;
    }

    public addMessage(content: string, sender: string, isSentFromLocal: boolean) {
        const currentDate = new Date();
        const formattedTime = currentDate.getHours() + ":" + String(currentDate.getMinutes()).padStart(2, "0");
        // TODO: Instead of the `isSentFromLocal`, create a uuid per person
        if (this.messageSets.length && this.messageSets[0].sender === sender && (this.messageSets[0].isSentFromLocal === isSentFromLocal)) {
            // Handling case when the last messageSet was sent from the sender
            this.messageSets[0].messages.push(content);
            return;
        }
        // Otherwise, need to create a new messageSet
        const msgSet = new MessageSet([content], sender, isSentFromLocal, formattedTime)
        this.messageSets.unshift(msgSet);
    }
}
