import './App.css';
import React from "react";

import MessageSet from "./components/chat/MessageSet"
import Registration from "./components/registration/Registration";
import InputAndSubmit from "./components/forms/InputAndSubmit";

import io from "socket.io-client";
import ChatBubbles from "./components/chat/ChatBubbles";
import {IMessagingProvider} from "./providers/MessagingProvider";
import {IUserProvider} from "./providers/UserProvider";
import {resolve} from 'inversify-react';
import Header from "./components/header/Header";


interface AppState {
    messageSets: MessageSet[],
    username: string | null,
    socket: any,
    currentlyTyping: string[]
}

class App extends React.Component<{}, AppState> {
    @resolve("messagingProvider") private readonly messagingProvider!: IMessagingProvider;
    @resolve("userProvider") private readonly userProvider!: IUserProvider;

    constructor(props: any) {
        super(props);
        this.state = {
            socket: io("/"),
            messageSets: [],
            username: null,
            currentlyTyping: []
        };
    }

    componentDidMount = () => {
        this.setState({
            username: this.userProvider.getUsername()
        })
        this.state.socket.on("new-message", (event: any) => {
            const {content, sender} = event;
            console.log("got new message from server", content, sender);
            this.messagingProvider.addMessage(content, sender, false);
            this.setState({
                messageSets: this.messagingProvider.getMessages()
            });
        });

        this.state.socket.on("currently-typing", async (event: any) => {
            if (this.state.currentlyTyping.length === event.usernames.length && this.state.currentlyTyping.every(username => event.usernames.indexOf(username))) {
                return;
            }
            this.setState({
                currentlyTyping: event.usernames.filter((username: string) => username !== this.state.username)
            });
        })

        window.addEventListener("send-message", (event: any) => {
            console.log("Sending a message", event.detail.content);
            this.state.socket.emit("new-message", {"content": event.detail.content, "sender": event.detail.sender});
            this.messagingProvider.addMessage(event.detail.content, event.detail.sender, true);
            const messages = this.messagingProvider.getMessages();
            this.setState({
                messageSets: messages
            });
        });

        window.addEventListener(
            "on-registration", _ => this.setState({username: this.userProvider.getUsername()})
        );

        window.addEventListener("on-logout", _ => this.setState({username: null}));

        window.addEventListener("typing", (event: any) => {
            this.state.socket.emit("typing", {"username": event.detail.username});
        })
    }

    notifyTyping(event: any) {
        if (event.key !== "Enter" || event.target.value === "") {
            window.dispatchEvent(new CustomEvent("typing", {
                detail: {
                    username: window.sessionStorage.getItem("username")
                }
            }));
            return;
        }
    }

    sendMessage(value: string) {
        if (!value) {
            return;
        }
        window.dispatchEvent(new CustomEvent("send-message", {
            detail: {
                content: value,
                sender: window.sessionStorage.getItem("username")
            }
        }));
    }

    renderCurrentlyTypingString(currentlyTyping: string[]) {
        if (!currentlyTyping.length) return "";
        if (currentlyTyping.length === 1) return `${currentlyTyping[0] + " is typing..."}`;
        let str = "";
        for (let i = 0; i < Math.min(currentlyTyping.length, 3); i++) {
            str += currentlyTyping[i];
            if (i !== Math.min(currentlyTyping.length, 3) - 1) str += ", ";
        }
        str += " are typing...";
        return str;
    }

    render() {
        return (
            <div className="Chat">
                {
                    !this.state.username
                        ? <Registration/>
                        : undefined
                }
                <Header username={this.state.username}/>
                <div className={"chat-content"}>
                    <div className={"empty-filler"}/>
                    <ChatBubbles messageSets={this.state.messageSets}/>
                    <div className={"empty-filler"} id={"bottom-of-chat"}/>
                </div>
                <div className={"chat-currently-typing"}>
                    {this.renderCurrentlyTypingString(this.state.currentlyTyping)}
                </div>
                <InputAndSubmit autoComplete={false} placeholder="Enter message"
                                onKeyPress={this.notifyTyping} onSubmit={this.sendMessage}/>
            </div>
        );
    }
}

export default App;
