import './InputAndSubmit.css';
import React, {useState} from "react";
import {IoSend} from "react-icons/io5";

interface InputAndSubmitProps {
    placeholder: string;
    autoComplete: boolean;
    onSubmit: CallableFunction;
    onKeyPress?: CallableFunction;
}

const InputAndSubmit = (props: InputAndSubmitProps) => {
    const [inputValue, setInputValue] = useState("");

    const onChange = (event: any) => {
        setInputValue(event.target.value);
        if (props.onKeyPress) {
            props.onKeyPress(event);
        }
    }

    const onSubmit = () => {
        props.onSubmit(inputValue);
        setInputValue("");
    }

    const _onKeyPress = (event: any) => {
        if (event.key === "Enter" && event.target.value !== "") { // Submit if it's necessary.
            onSubmit();
        }
    }

    return (
        <div className={"input-and-submit-wrapper"}>
            <input autoComplete={props.autoComplete ? "" : "off"}
                   value={inputValue}
                   onChange={onChange}
                   onKeyPress={_onKeyPress}
                   onSubmit={onSubmit}
                   placeholder={props.placeholder} type={"text"}
                   id={"new-message"}/>
            <div onClick={onSubmit} className="send-button-wrap">
                <IoSend id={"send-button"} fontSize={"1.5em"}/>
            </div>
        </div>
    )
}

export default InputAndSubmit;
