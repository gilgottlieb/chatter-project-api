import './Header.css';
import React from "react";
import {FiMoreVertical, IoLogOut} from "react-icons/all";
import {IconButton, ListItemIcon, Menu, MenuItem} from "@mui/material";
import {IUserProvider} from "../../providers/UserProvider";
import {useInjection} from "inversify-react";

interface HeaderProps {
    username: string | null
}

const Header = (props: HeaderProps) => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const isMenuOpen = Boolean(anchorEl);

    const userProvider = useInjection<IUserProvider>('userProvider');

    const handleMenuButtonClick = (event: any) => {
        setAnchorEl(event.currentTarget);
    };

    const handleMenuButtonCloseClick = () => {
        setAnchorEl(null);
    };

    const onLogoutButtonPress = async () => {
        handleMenuButtonCloseClick();
        await userProvider.logout();
    }

    return (
        <div className="header">
            <div className={"header-left-side"}>
                <div className={"title"}>
                    Chatter
                </div>
            </div>
            <div className={"header-right-side"}>
                {
                    props.username
                        ?
                        <div className={"logged-user-info"}>
                            {props.username}
                            <IconButton
                                aria-label="more"
                                id="long-button"
                                aria-controls="long-menu"
                                aria-expanded={isMenuOpen ? 'true' : undefined}
                                aria-haspopup="true"
                                onClick={handleMenuButtonClick}
                            >
                                <FiMoreVertical/>
                            </IconButton>
                            <Menu
                                id="long-menu"
                                MenuListProps={{
                                    'aria-labelledby': 'long-button',
                                }}
                                anchorEl={anchorEl}
                                open={isMenuOpen}
                                onClose={handleMenuButtonCloseClick}
                                PaperProps={{
                                    style: {
                                        width: '20ch',
                                    },
                                }}
                            >
                                <MenuItem key={'logout'} onClick={onLogoutButtonPress}>
                                    <ListItemIcon>
                                        <IoLogOut fontSize={"1.5em"} />
                                    </ListItemIcon>
                                    Logout
                                </MenuItem>
                            </Menu>
                        </div>
                        : null
                }
            </div>
        </div>
    )
};

export default Header;
