import './ChatBubble.css';
import React from "react";
import MessageSet from "./MessageSet";

interface ChatBubbleProps {
    messageSet: MessageSet;
}

const ChatBubble = (props: ChatBubbleProps) => {

    const messages = props.messageSet.messages.map(msg => <div key={Math.random()}
                                                                    className="single-message">{msg}</div>);
    return (
        <div className={"chat-bubble-wrapper " + (props.messageSet.isSentFromLocal ? "left" : "right")}>
            <div className={"chat-bubble-time"}>
                {props.messageSet.time}
            </div>
            <div className={"chat-bubble"}>
                {
                    !props.messageSet.isSentFromLocal
                        ? <div className={"sender-name"}>
                            {props.messageSet.sender}
                        </div> : undefined
                }

                {messages}
            </div>
        </div>
    );
}

export default ChatBubble;
