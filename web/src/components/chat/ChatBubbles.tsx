import React from "react";
import MessageSet from "./MessageSet";
import ChatBubble from "./ChatBubble";

interface ChatBubblesProps {
    messageSets: MessageSet[]
}

const ChatBubbles = (props: ChatBubblesProps) => {
    return <>{props.messageSets.map(
        set => <ChatBubble key={Math.random()}
                           messageSet={set}/>
    )}</>
}

export default ChatBubbles;
