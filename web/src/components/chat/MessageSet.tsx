class MessageSet {
    messages: string[];
    sender: string;
    isSentFromLocal: boolean;
    time: string;
    constructor(messages: string[], sender: string, isSentFromLocal: boolean, time: string) {
        this.messages = messages;
        this.sender = sender;
        this.isSentFromLocal = isSentFromLocal;
        this.time = time;
    }
}

export default MessageSet;
