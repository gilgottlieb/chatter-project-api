import './Registration.css';
import React from "react";
import InputAndSubmit from "../forms/InputAndSubmit";
import {IUserProvider} from "../../providers/UserProvider";
import {resolve} from "inversify-react";


class Registration extends React.Component {
    @resolve("userProvider") private readonly userProvider!: IUserProvider;

    componentDidMount() {
    }

    async onSubmit (value: string) {
        await this.userProvider.login(value);
        window.dispatchEvent(new CustomEvent("on-registration"));
    }

    render() {
        return (
            <div className="registration-background">
                <div className={"title"}>
                    Chatter
                </div>
                <div className="registration-window">
                    <div className="registration-text">
                        <b>Hi!</b><br/>
                        To start chatting, please enter your name
                    </div>
                    <InputAndSubmit autoComplete={false} placeholder="Enter here..." onSubmit={this.onSubmit.bind(this)} />
                </div>
            </div>
        )
    }
}

export default Registration;
