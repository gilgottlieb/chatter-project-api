import "reflect-metadata";
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'inversify-react';
import { container } from './ioc';


ReactDOM.render(
    <React.StrictMode>
        <Provider container={container}>
            <App/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);
