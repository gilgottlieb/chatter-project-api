# Chatter
Chatter is a chat platform supports adding services such as bots on demand.  
We currently have Pricey, a bot that can help you remember your favourite items' prices.  
You can start testing it by sending the message `/pricey help`.

# Running Chatter
1. Run the product on your local machine
2. Running the server:
   1. Start by setting these environment variables for the Elastic:
      1. `ELASTIC_HOST`
      2. `ELASTIC_USERNAME`
      3. `ELASTIC_PASSWORD`
   2. Run the server by running: `cd api && npm install && npm start`
3. Run the client by running: `cd web && npm install && npm start`
