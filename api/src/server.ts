import {TypingIndicator} from "./services/typingIndicator";
import {NewMessageService} from "./services/newMessageService";
import express from "express";
import {PricesBotService} from "./services/pricesBotService";

// Initialising the backend API
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());

const server = app.listen(3080, "0.0.0.0", () => {
    console.log("server listening on port 3080")
})

// Initialising the socket
const socket = require("socket.io");
const io = socket(server);
const typingIndicatorService = new TypingIndicator()
const services = [
    typingIndicatorService,
    new NewMessageService(typingIndicatorService),
    new PricesBotService(process.env.ELASTIC_HOST, process.env.ELASTIC_USERNAME, process.env.ELASTIC_PASSWORD, typingIndicatorService)
]

io.on("connection", (socket: any) => {
    // TODO: Known error - If multiple users will create a connection at the same time when the server starts, there might be race condition
    //  that the new message service would be initialised multiple times
    for (const service of services) {
        if (service.shouldInitialise()) {
            service.initialise(socket);
        }
    }
})
