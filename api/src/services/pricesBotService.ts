import {Client} from '@elastic/elasticsearch';
import {TypingIndicator} from "./typingIndicator";

interface Price {
    itemName: string
    price: number
}

class RegexMatch {
    regex: RegExp
    action: CallableFunction

    constructor(regex: RegExp, action: CallableFunction) {
        this.regex = regex;
        this.action = action;
    }
}

export class PricesBotService {
    socket: any;
    pricesList: Price[];
    regexMatches: RegexMatch[];
    elasticClient: Client;
    private typingIndicatorService: TypingIndicator;

    constructor(elasticHost: string, elasticUsername: string, elasticPassword: string, typingIndicatorService: TypingIndicator) {
        this.typingIndicatorService = typingIndicatorService;
        this.socket = null;
        this.elasticClient = new Client({
            node: elasticHost,
            auth: {
                username: elasticUsername,
                password: elasticPassword
            }
        });

        this.regexMatches = [
            new RegexMatch(/\/pricey get "(?<product>.*)"/, async (match: any) => {
                const elasticResult = await this.elasticClient.search({
                    index: "prices",
                    body: {
                        query: {match: {productName: match.groups["product"]}},
                        sort: [{ "_score" : "desc" }, { "datetime" : "desc" }]
                    }
                });
                if (!elasticResult.body.hits.hits.length) {
                    return `I don't know the price of ${match.groups["product"]}, care to teach me?
                    Just write: /pricey update "${match.groups["product"]}" REPLACE_WITH_PRICE`
                }
                if (elasticResult.body.hits.hits.length === 1) {
                    const product = elasticResult.body.hits.hits[0];
                    const {productName, productPrice} = product["_source"];
                    return `I've only found the price for "${productName}" which is ${productPrice}.
                    If it's not the one, how about teaching me?`
                }
                const amountOfHits = elasticResult.body.hits.hits.length;
                const returnedResults = elasticResult.body.hits.hits.slice(0, 2);
                return `I found ${amountOfHits} possible results, here are a couple of them:\n` + returnedResults.map((product: any) => {
                    const {productName, productPrice} = product["_source"];

                    return `"${productName}" costs ${productPrice}.\n`;
                }).join("") + `
                If none of those products are relevant to your query, please help me become a smarter being and teach me the price of ${match.groups["product"]}.
                /pricey update "${match.groups["product"]}" ENTER_PRICE_HERE`;
            }),
            new RegexMatch(/\/pricey update "(?<product>.*)" (?<price>.*)/, async (match: any) => {
                const product = match.groups["product"],
                    price = match.groups["price"];
                this.pricesList[product] = price;
                await this.elasticClient.index({
                    index: 'prices',
                    body: {
                        "productName": product,
                        "productPrice": price,
                        "datetime": new Date()
                    }
                });

                return `I've updated the price of ${product} to ${price}, thanks for the collaboration!`
            }),
            new RegexMatch(/\/pricey help/, async (match: any) => {
                return `Hey there, I'm always here to help you remember the prices for your favourite products! You can send:
                /pricey update "PRODUCT_NAME" PRODUCT_PRICE
                /pricey get "PRODUCT_NAME"
                
                Good luck!`
            })
        ]
        this.pricesList = [];
    }

    async _getPriceysResponse(message: string) {
        try {
            this.typingIndicatorService.addToList("Pricey");
            for (const regexMatch of this.regexMatches) {
                const match = message.match(regexMatch.regex);
                if (match) {
                    return await regexMatch.action(match)
                }
            }
            return `Pricey couldn't parse the message! You can write /pricey help`;
        }
        catch (e) {
            console.log("Got an exception while trying to get Pricey's response", e);
        }
        finally {
            this.typingIndicatorService.removeFromList("Pricey");
        }
    }

    initialise(socket: any) {
        socket.on("new-message", async ({content, sender}: any) => {
            if (!content.startsWith("/pricey")) return;

            const priceysResponse = {
                content: await this._getPriceysResponse(content),
                sender: "Pricey"
            }
            socket.broadcast.emit("new-message", priceysResponse);
            socket.emit("new-message", priceysResponse);
        });
    }

    shouldInitialise() {
        return this.socket === null
    }
}
