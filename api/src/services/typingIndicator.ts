interface CurrentlyTyping {
    username: string
    datetime?: Date
}

export class TypingIndicator {
    socket: any;
    currentlyTyping: CurrentlyTyping[]

    constructor() {
        this.socket = null;
        this.currentlyTyping = [];
    }

    initialise(socket: any) {
        socket.on("typing", ({username}: any) => {
            const userBlockIndex = this.currentlyTyping.findIndex(obj => obj.username === username);
            if (userBlockIndex === -1) {
                this.currentlyTyping.push({
                    username: username,
                    datetime: new Date()
                });
            } else {
                this.currentlyTyping[userBlockIndex]["datetime"] = new Date();
            }
        });

        setInterval(() => {
            this.currentlyTyping = (
                this.currentlyTyping.filter(typingIndicator => !typingIndicator.datetime || ((new Date()).getTime() - typingIndicator.datetime.getTime()) < 2 * 1000) || []
            )
            socket.broadcast.emit("currently-typing", {
                usernames: this.currentlyTyping.map(typingIndicator => typingIndicator.username)
            });
        }, 2000);
    }

    shouldInitialise() {
        return this.socket === null
    }

    addToList(username: string) {
        this.currentlyTyping.push({
            username
        });
    }

    removeFromList(username: string) {
        this.currentlyTyping = this.currentlyTyping.filter(typingIndicator => typingIndicator.username !== username) || []
    }
}
