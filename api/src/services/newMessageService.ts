import {TypingIndicator} from "./typingIndicator";

export class NewMessageService {
    socket: any;
    private typingIndicatorService: TypingIndicator;

    constructor(typingIndicatorService: TypingIndicator) {
        this.socket = null;
        this.typingIndicatorService = typingIndicatorService;
    }

    initialise(socket: any) {
        socket.on("new-message", ({content, sender}: any) => {
            socket.broadcast.emit("new-message", {
                content,
                sender
            });
            this.typingIndicatorService.removeFromList(sender);
        });
    }

    shouldInitialise() {
        return this.socket === null
    }
}
